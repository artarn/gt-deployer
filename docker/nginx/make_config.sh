#!/usr/bin/env bash
export DEFAULT_CONFIG_FILE="/etc/nginx/conf.d/default.conf"
export COMPILED_CONFIG="/tmp/default.conf"

echo "" > $COMPILED_CONFIG;

for cfg in /nginx_configs/*.conf; do
    cat "$cfg" >> $COMPILED_CONFIG;
    echo "" >> $COMPILED_CONFIG;
done

sed -i -E 's/\$([a-z])/${dollar}\1/g' $COMPILED_CONFIG

export dollar='$'

envsubst < $COMPILED_CONFIG > $DEFAULT_CONFIG_FILE
